package archive;

import cz.cvut.eshop.archive.ItemPurchaseArchiveEntry;
import cz.cvut.eshop.shop.Item;
import cz.cvut.eshop.shop.StandardItem;
import org.junit.Assert;
import org.junit.Test;

public class ItemPurchaseArchiveEntryTest {

    @Test
    public void ItemPurchaseArchiveEntry_Not_Null_Item(){
        Item item = new StandardItem(1, "item 1", 100, "category 1", 5);
        ItemPurchaseArchiveEntry ipae = new ItemPurchaseArchiveEntry(item);
        Assert.assertEquals(item, ipae.getRefItem());
    }

    @Test(expected = IllegalArgumentException.class)
    public void ItemPurchaseArchiveEntry_Null_Item(){
        ItemPurchaseArchiveEntry ipae = new ItemPurchaseArchiveEntry(null);
    }

    @Test
    public void ItemPurchaseArchiveEntry_Object_Created(){
        Item item = new StandardItem(1, "item 1", 100, "category 1", 5);
        ItemPurchaseArchiveEntry ipae = new ItemPurchaseArchiveEntry(item);
        Assert.assertNotEquals(null, ipae);
    }

    @Test
    public void increaseCountHowManyTimesHasBeenSold_Positive(){
        Item item = new StandardItem(1, "item 1", 100, "category 1", 5);
        ItemPurchaseArchiveEntry ipae = new ItemPurchaseArchiveEntry(item);
        ipae.increaseCountHowManyTimesHasBeenSold(10);
        Assert.assertEquals(11, ipae.getCountHowManyTimesHasBeenSold());
    }

    @Test(expected = IllegalArgumentException.class)
    public void increaseCountHowManyTimesHasBeenSold_Negative(){
        Item item = new StandardItem(1, "item 1", 100, "category 1", 5);
        ItemPurchaseArchiveEntry ipae = new ItemPurchaseArchiveEntry(item);
        ipae.increaseCountHowManyTimesHasBeenSold(-5);
    }

    @Test
    public void toStringTest(){
        Item item = new StandardItem(1, "item 1", 100, "category 1", 5);
        ItemPurchaseArchiveEntry ipae = new ItemPurchaseArchiveEntry(item);
        String expectedString = "ITEM  "+item.toString()+"   HAS BEEN SOLD "+1+" TIMES";
        Assert.assertEquals(expectedString, ipae.toString());
    }

}
